package INF102.lab1.triplicate;

import java.util.Collections;
import java.util.List;

public class MyTriplicate<T extends Comparable<T>> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        // Implement me :)
        Collections.sort(list);
        for (int i = 2 ; i < list.size() ; i++) {
            if (list.get(i).equals(list.get(i-1)) && list.get(i-1).equals(list.get(i-2))) {
                return list.get(i);
            }
        }
        return null;
    }
    
}
